package com.ecommerce.payement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
@EnableFeignClients
@SpringBootApplication

public class PayementApplication {

    public static void main(String[] args) {
        SpringApplication.run(PayementApplication.class, args);
    }

}
