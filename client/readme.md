#### **Client Microservice EndPoints**

These are the requests permited by this MicroService with a bief description.

| Path      |Type| Function                       |
|:--------------|:---|:----------------------------------|
| `/client`      | `POST`|add a client to the data base
| `/clientPay/{id}`    |`GET`| allow he client to pay a command having the ID=id
| `/client/{id}` |`POST`  |return a JSON object with the client of the ID=id
| `/commandes/{id}`     | `GET`|  return a JSON array contain all the commandes done by client having ID=id|
| `/products`      | `GET`|return all products
| `/PasserUneCommande`      | `POST`| pass a command
**_The application is running on port 8080_**