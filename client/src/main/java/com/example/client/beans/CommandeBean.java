package com.example.client.beans;

import lombok.*;

import java.math.BigDecimal;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class CommandeBean {

    private Long id;

    private Long idClient;

    private Long idProduct;

    private Date dateCommande ;

    private BigDecimal amount ;

    private int quantity ;

    private boolean isPayed =false  ;

}
