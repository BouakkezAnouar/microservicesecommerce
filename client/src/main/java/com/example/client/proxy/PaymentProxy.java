package com.example.client.proxy;


import com.example.client.beans.CommandeBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "paymentMicroservice" , url="localhost:83")
public interface PaymentProxy {

    @GetMapping(value = "/payment/{id}")
    public void payerUneCommande(@PathVariable Long id) ;
}
