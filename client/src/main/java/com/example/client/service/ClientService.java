package com.example.client.service;

import com.example.client.beans.CommandeBean;
import com.example.client.beans.ProductBean;
import com.example.client.model.Client;
import com.example.client.proxy.CommandeProxy;
import com.example.client.proxy.PaymentProxy;
import com.example.client.proxy.ProductProxy;
import com.example.client.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository ;

    @Autowired
    CommandeProxy commandeProxy ;

    @Autowired
    PaymentProxy paymentProxy;

    @Autowired
    ProductProxy productProxy;


    public Client getClient (Long id ){
        return clientRepository.findById(id).get();
    }

    public List<Client> getAllClients (){
        return clientRepository.findAll();
    }

    public List<CommandeBean> getAllCommandes (Long id ){

        return commandeProxy.commandesOfClient(id);

    }

    public Client addClient ( Client client ){

        return clientRepository.save(client);
    }

    public void payerCommande (Long id){

        paymentProxy.payerUneCommande(id);
    }

    public List<ProductBean> getAllProducts (){

        return productProxy.getAllProducts();
    }

    public CommandeBean passcommande (CommandeBean  comm){


        return commandeProxy.passCommande(comm).getBody();
    }

}
