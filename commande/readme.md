#### **Commande Microservice EndPoints**

These are the requests permited by this MicroService with a bief description.

| Path      |Type| Function                       |
|:--------------|:---|:----------------------------------|
| `/commandes`      | `GET`|returns a json array that contain all the commandes in the database
| `/commandes/{id}`    |`GET`| return the commande with the Id=id in a json format
| `/commandes` |`POST`  |Pass a commande with the json passed in request body
| `/commandes/{id}`     | `DELETE`| Delete the command having an ID=id|
| `/commandes`     | `UPDATE`| Update the command with the json passed in request body

**_The application is running on port 82_**