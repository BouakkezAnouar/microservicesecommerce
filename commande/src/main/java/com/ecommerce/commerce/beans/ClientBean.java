package com.ecommerce.commerce.beans;

import lombok.*;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class ClientBean {

    Long id ;

    String name ;
}
