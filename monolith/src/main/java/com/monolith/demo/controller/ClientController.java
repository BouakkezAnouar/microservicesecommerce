package com.monolith.demo.controller;

import com.monolith.demo.model.Commande;
import com.monolith.demo.model.Product;
import com.monolith.demo.model.Client;
import com.monolith.demo.service.ClientService;
import com.monolith.demo.service.CommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/")
public class ClientController {

    @Autowired
    ClientService clientService ;

    @GetMapping(value = "clientPay/{idCommande}")
    void payer (Long idCommande){
        clientService.payerCommande (idCommande);
    }

    @GetMapping(value = "/client/{id}")
    public  Client getClient (@PathVariable Long id){
        return clientService.getClient(id);
    }

    @GetMapping ( value = "commandes/{idClient}")
    List<Commande> getAllCommandes (@PathVariable Long idClient){
        return  clientService.getAllCommandes(idClient);
    }

    @PostMapping(value = "client")
    Client addClient (@RequestBody Client client){

        return clientService.addClient(client);
    }

    @GetMapping(value = "products")
    List<Product> getAllProducts() {
        return clientService.getAllProducts();
    }

    @PostMapping(value = "PasserUneCommande")
    Commande passerUneCommande(@RequestBody  Commande comm){
        return clientService.passcommande(comm);
    }

}

