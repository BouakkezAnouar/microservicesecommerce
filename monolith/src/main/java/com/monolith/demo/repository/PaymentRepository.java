package com.monolith.demo.repository;


import com.monolith.demo.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository <Payment, Long> {
    Payment findByidCommande(Long idCommande);
}
