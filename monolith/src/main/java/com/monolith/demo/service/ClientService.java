package com.monolith.demo.service;


import com.monolith.demo.controller.CommandeController;
import com.monolith.demo.controller.PaymentController;
import com.monolith.demo.controller.ProductController;
import com.monolith.demo.model.Client;
import com.monolith.demo.model.Commande;
import com.monolith.demo.model.Product;
import com.monolith.demo.repository.ClientRepository;
import com.monolith.demo.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository ;

    @Autowired
    CommandeController commandeController ;

    @Autowired
    PaymentController paymentController;

    @Autowired
    ProductController productController;


    public Client getClient (Long id ){
        return clientRepository.findById(id).get();
    }

    public List<Client> getAllClients (){
        return clientRepository.findAll();
    }

    public List<Commande> getAllCommandes (Long id ){

        return  commandeController.allCommandes();

    }

    public Client addClient ( Client client ){

        return clientRepository.save(client);
    }

    public void payerCommande (Long id){

        paymentController.payerUneCommande(id);
    }

    public List<Product> getAllProducts (){

        return productController.getAllProducts();
    }

    public Commande passcommande (Commande  comm){


        return commandeController.passCommande(comm).getBody();
    }

}
