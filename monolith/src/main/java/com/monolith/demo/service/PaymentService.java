package com.monolith.demo.service;


import com.monolith.demo.model.Client;
import com.monolith.demo.model.Commande;
import com.monolith.demo.model.Product;
import com.monolith.demo.repository.ClientRepository;
import com.monolith.demo.repository.CommandeRepository;
import com.monolith.demo.repository.PaymentRepository;
import com.monolith.demo.model.Payment;
import com.monolith.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static com.monolith.demo.controller.ClientController.*;

@Service
public class PaymentService {
    @Autowired
    private PaymentRepository paymentRepository ;
    @Autowired
    private CommandeRepository commandeRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ClientRepository clientRepository;
    public void payerCommande(Long idCommande)
    {
        //avoir commande en utilisant id commande
        Commande commande=commandeRepository.findById(idCommande).get();
        System.out.println(commande);
        //consulter idClient
        Long idClient = commande.getIdClient();
        Payment payment =new Payment();
        Client client= clientRepository.getOne(idClient);
        //solde du client
        BigDecimal soldeClient= client.getSolde();
        //comparer le solde du client au valeur de la commande
        if(soldeClient.compareTo(commande.getAmount())==1) {
            System.out.println("goooooollllll gooooollllll");
            commande.setPayed(true);
            commande.update(commande);

            int quantity = commande.getQuantity();
            Long idProduit = commande.getIdProduct();
            System.out.println(idProduit);
            Product produit= productRepository.getOne(idProduit);
            produit.setStockquantity(Math.abs(produit.getStockquantity()-quantity));
            //modify product
            productRepository.save(produit);

            Client clientb = clientRepository.getOne(idClient);
            clientb.setSolde(soldeClient.subtract(commande.getAmount()));
            clientRepository.save(clientb);

            payment.setQuantity(quantity);
            payment.setIdCommande(idCommande);
            payment.setIdClient(idClient);
            payment.setIdProduit(idProduit);
            paymentRepository.save(payment);
        }
    }
    public List<Product> TestProduit()
    {
        return productRepository.findAll();
    }
    public List <Commande> TestCommande()
    {
        return commandeRepository.findAll();
    }
}
