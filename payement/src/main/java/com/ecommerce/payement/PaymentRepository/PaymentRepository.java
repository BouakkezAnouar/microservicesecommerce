package com.ecommerce.payement.PaymentRepository;

import com.ecommerce.payement.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository <Payment, Long> {
    Payment findByidCommande(Long idCommande);
}
