package com.ecommerce.payement.PaymentService;

import com.ecommerce.payement.PaymentRepository.PaymentRepository;
import com.ecommerce.payement.beans.ClientBean;
import com.ecommerce.payement.beans.CommandeBean;
import com.ecommerce.payement.beans.ProduitBean;
import com.ecommerce.payement.model.Payment;
import com.ecommerce.payement.proxy.MicroserviceClientProxy;
import com.ecommerce.payement.proxy.MicroserviceCommandeProxy;
import com.ecommerce.payement.proxy.MicroserviceProduitProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;
    @Autowired
    private MicroserviceCommandeProxy microserviceCommandeProxy;
    @Autowired
    private MicroserviceProduitProxy microserviceProduitProxy;
    @Autowired
    private MicroserviceClientProxy microserviceClientProxy;
    public void payerCommande(Long idCommande)
    {
        //avoir commande en utilisant id commande
        CommandeBean commande=microserviceCommandeProxy.getCommande(idCommande).getBody();
       // System.out.println(commande);
        //consulter idClient
        Long idClient = commande.getIdClient();
        Payment payment =new Payment();
        Optional <ClientBean> client= microserviceClientProxy.getClient(idClient);
        //System.out.print(client);
        //solde du client
        BigDecimal soldeClient= client.get().getSolde();
        //comparer le solde du client au valeur de la commande
        if(soldeClient.compareTo(commande.getAmount())==1) {

            System.out.println(commande);
            commande.setPayed(true);
            System.out.println(commande);
            System.out.println(microserviceCommandeProxy.updateCommande(commande));

            int quantity = commande.getQuantity();
            Long idProduit = commande.getIdProduct();
            ProduitBean produit= microserviceProduitProxy.getProduct(idProduit).getBody();
            produit.setStockquantity(Math.abs(produit.getStockquantity()-quantity));
            microserviceProduitProxy.addproduct(produit);
            //
            ClientBean clientb = microserviceClientProxy.getClient(idClient).get();
            clientb.setSolde(soldeClient.subtract(commande.getAmount()));
            microserviceClientProxy.addClient(clientb);

            payment.setQuantity(quantity);
            payment.setIdCommande(idCommande);
            payment.setIdClient(idClient);
            payment.setIdProduit(idProduit);
            paymentRepository.save(payment);
        }
    }
    public List<ProduitBean> TestProduit()
    {
        return microserviceProduitProxy.getAllProducts();
    }
    public List <CommandeBean> TestCommande()
    {
        return microserviceCommandeProxy.allCommandes();
    }
}
